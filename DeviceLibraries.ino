enum DeviceMode {
        DEVICE_MODE_DEFAULT = 0,
        DEVICE_MODE_SETUP = 1,
        DEVICE_MODE_WORK = 2,
        DEVICE_MODE_FAILURE = 4
    };

enum DeviceErrorType {
        DEVICE_OK = 0b00000000,
        DEVICE_FATAL_ERROR = 0b10000001,
        DEVICE_INVALID_STATE = 0b00000010,
        DEVICE_SENSOR_ERROR = 0b00000011
    };


struct DeviceState {
    char UID[12];
    DeviceMode mode;
    DeviceErrorType errorCode;
    char errorReason[256];
    char name[64];
};

DeviceState defaultDeviceState = {
    {.UID = "undefined" },
    .mode = DeviceMode::DEVICE_MODE_SETUP,
    .errorCode = DeviceErrorType::DEVICE_OK,
    {.errorReason = "" },
    {.name = "noname"}
};



void setup() {

}

void loop() {
}
