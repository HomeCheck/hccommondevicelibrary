Write-Output "`nHomecheck(TM) Setup - 2021`n"
Write-Output "This will create a symlink for DeviceLibraries to your Arduino library. "
Write-Output "It makes available to use HomeCheck common device libraries for development.`n"

$defaultLibraryFolder = "E:\Projects\Arduino\libraries"
$defaultSourceFolder = "E:\Projects\HomeCheck.hu"

$libraryFolder = Read-Host -Prompt "Enter Arduino Library folder ($defaultLibraryFolder)"

if ($libraryFolder -eq "") {
    $libraryFolder = $defaultLibraryFolder
}

if (-Not (Test-Path -Path $libraryFolder)) {
    Write-Output "folder $libraryFolder does not exist. Exiting script..."
    exit
}

$sourceFolder = Read-Host -Prompt "Enter HC Library folder ($defaultSourceFolder)"

if ($sourceFolder -eq "") {
    $sourceFolder = $defaultSourceFolder
    Write-Host "using default folder: $sourceFolder"
} else {
    Write-Host "using entered dir: $sourceFolder"
}

# ----------------------------------------------------------------------------------------------------
Write-Output "`nCreating symlink for HomeCheck common library...`n"

$commonLibrarySymLinkPath = "$libraryFolder\HomeCheck"
$commonLibrarySourcePath = "$sourceFolder\DeviceLibraries\src"

if (-Not(Test-Path -Path $commonLibrarySourcePath)) {
    Write-Output "Source folder does not exist. Exiting script..."
    exit
}

if (Test-Path -Path $commonLibrarySymLinkPath) {
    Write-Output "Symlink already exist"
} else {
    New-Item -ItemType SymbolicLink -Path $commonLibrarySymLinkPath -Target $commonLibrarySourcePath
}

# ----------------------------------------------------------------------------------------------------
Write-Output "`nCreating symlink for WateringStation library...`n"

$wateringStationSymLinkPath = "$libraryFolder\WateringStation"
$wateringStationSourcePath = "$sourceFolder\Devices\WateringStation"

if (-Not(Test-Path -Path $wateringStationSourcePath)) {
    Write-Output "Source folder does not exist. Exiting script..."
    exit
}

if (Test-Path -Path $wateringStationSymLinkPath) {
    Write-Output "Symlink already exist"
} else {
    New-Item -ItemType SymbolicLink -Path $wateringStationSymLinkPath -Target $wateringStationSourcePath
}
