#pragma once

#include "ValueMap.h"
#include "Preferences.h"
#include "ESP32Tools.h"
#include "HCCommon.h"
#include "ESP.h"
#include <map>

namespace HC {
    ValueMap::ValueMap(const char* pNamespace) {
        log_d("allowPersistence for %s is %s", pNamespace, allowPersistence ? "enabled" : "disabled");

        this->allowPersistence = strlen(pNamespace) > 0;

        if (this->allowPersistence) {
            preferences = new Preferences();
            strcpy(this->prefNamespace, pNamespace);
            log_d("Preferences for %s created", pNamespace);
        }
    }

    ValueMap::~ValueMap() {
        this->preferences->end();
    }

    bool ValueMap::isAllowed(String name) {
        return !onlyAllowed || (allowedKeys.find(name) != allowedKeys.end() && onlyAllowed);
    }

    void ValueMap::set(const char* name, const char* value) {
        if (isAllowed(name)) {
            params[name] = value;
        } else {
            log_v("key not allowed: '%s'", name);
        }
    }

    void ValueMap::addDefault(const char* name, const char* value) {
        if (isAllowed(name)) {
            defaults[name] = value;
        } else {
            log_v("default key not allowed: '%s'", name);
        }
    }

    void ValueMap::remove(const char*  name) {
        params.erase(name);
    }

    String ValueMap::get(const char* name) {
        return params[name];
    }

    void ValueMap::addAllowedKey(const char* name) {
        allowedKeys.insert(name);
    }

    void ValueMap::addAllowedKeyAndDefault(const char* name, const char* value) {
        addAllowedKey(name);
        addDefault(name, value);
    }

    ValuePairs* ValueMap::getAll() {
        return &params;
    }
    void ValueMap::resetToDefaults() {
        params.clear();

        for (auto const& item : defaults) {
            params[item.first] = item.second;
        }
    }

    void ValueMap::load() {
        if (allowPersistence) {
            preferences->begin(prefNamespace);

            for (auto const& keyStr : allowedKeys) {
                // maximum length of a param is MAX_PARAM_LENGTH, can't be changed
                char readValue[MAX_DEVICE_PARAM_VALUE_LENGTH];
                preferences->getString(keyStr.c_str(), readValue, MAX_DEVICE_PARAM_VALUE_LENGTH);

                // preferences getString with defaultValue argument does not work properly
                // so load manage setting default value if param is empty
                // TODO: check whether it is ok or not to check length for existence.
                if (strlen(readValue) == 0) {
                    strcpy(readValue, this->defaults[keyStr].c_str());
                };

                this->set(keyStr.c_str(), readValue);
            }
        }
    }

    void ValueMap::save() {
        if (allowPersistence) {
            preferences->begin(prefNamespace);

            for (auto const& keyStr : allowedKeys) {
                String value = this->get(keyStr.c_str());
                preferences->putString(keyStr.c_str(), value.c_str());
            }
        }
    }
}