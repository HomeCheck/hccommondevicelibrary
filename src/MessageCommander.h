#pragma once

#include "HCCommon.h"
#include "PubSubClient.h"
#include <map>

namespace HC {
    struct MessageCommanderConfig {
        PubSubClient& mqttClient;
    };

    typedef std::function<bool(const char*, const ValuePairs&)> CommandFn;
    typedef std::map<String, CommandFn> CommandMap;

    class MessageCommanderClass {
    private:
        PubSubClient& mqttClient;
        CommandMap commands;
    protected:
    public:
        MessageCommanderClass(MessageCommanderConfig& config);

       void addCommand(const char* commandName, CommandFn callback);
       bool callCommand(const char* requestId, const char* commandName, const ValuePairs& values);
       bool hasCommand(const char* commandName);
       void printCommands();
    };
}