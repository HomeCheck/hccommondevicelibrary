#pragma once

#include "DeviceStateStore.h"
#include "ConnectionDataStore.h"
#include "ESP32Tools.h"
#include "SPIFFS.h"
#include "BaseDevice.h"

namespace HC {
    void printESPInfo() {

        Serial.printf("+-----------------------+--------------+------+\n");
        Serial.printf("| Data                  |Value         |Unit  |\n");
        Serial.printf("+-[ Memory Info ]-------+--------------+------+\n");
        Serial.printf("|Total heap             | %12d | bytes|\n", ESP.getHeapSize());
        Serial.printf("|Free heap              | %12d | bytes|\n", ESP.getFreeHeap());
        Serial.printf("+-[ PSRAM Info ]--------+--------------+------+\n");
        Serial.printf("|Total PSRAM            | %12d | bytes|\n", ESP.getPsramSize());
        Serial.printf("|Free PSRAM             | %12d | bytes|\n", ESP.getFreePsram());
        Serial.printf("+-[ Flash Info ]--------+--------------+------+\n");
        Serial.printf("|Free sketch space      | %12d | bytes|\n", ESP.getFreeSketchSpace());
        Serial.printf("|Uploaded sketch size   | %12d | bytes|\n", ESP.getSketchSize());
        Serial.printf("|Flash Chip Mode        | %12d |      |\n", ESP.getFlashChipMode());
        Serial.printf("|Flash Chip Size        | %12d | bytes|\n", ESP.getFlashChipSize());
        Serial.printf("|Flash Chip Speed       | %12d | MHz  |\n", ESP.getFlashChipSpeed() / 1000000);
        Serial.printf("+-[ CPU Info ]----------+--------------+------+\n");
        Serial.printf("|Chip revision          | %12d |      |\n", ESP.getChipRevision());
        Serial.printf("|CPU Frequency          | %12d | MHz  |\n", ESP.getCpuFreqMHz());
        Serial.printf("+-[ SPIFFS Info ]-------+--------------+------+\n");
        Serial.printf("|SPIFFS total size      | %12d | bytes|\n", SPIFFS.totalBytes());
        Serial.printf("|SPIFFS used bytes      | %12d | bytes|\n", SPIFFS.usedBytes());
        Serial.printf("+-----------------------+--------------+------+\n");
    }


    void printHCStatusInfo() {
        HC::DeviceStateStore.begin();
        HC::DeviceState* state = HC::DeviceStateStore.load();
        Serial.printf("+---------------------------------------------+\n");
        Serial.printf("+ Device State                                +\n");
        Serial.printf("+-----------------------+---------------------+\n");
        Serial.printf("| Data                  |Value                |\n");
        Serial.printf("+-----------------------+---------------------+\n");
        Serial.printf("| UID                   | %s\n", HC::DeviceStateStore.getUID());
        Serial.printf("| Device name           | %s\n", HC::DeviceStateStore.getName());
        Serial.printf("| Error Code            | %d\n", HC::DeviceStateStore.getErrorCode());
        Serial.printf("| Error Reason          | %s\n", HC::DeviceStateStore.getErrorReason());
        Serial.printf("| Firmware Version      | %s\n", HC::DeviceStateStore.getFirmwareVersion());

        HC::DeviceMode mode = HC::DeviceStateStore.getMode();
        char* modeStr;
        switch (mode) {
        case 0:
            modeStr = "default";
            break;
        case 1:
            modeStr = "setup";
            break;
        case 2:
            modeStr = "work";
            break;
        case 3:
            modeStr = "failure";
            break;
        default:
            modeStr = "invalid mode";
            break;
        }

        Serial.printf("| Mode                  | %s\n", modeStr);
        Serial.printf("+-----------------------+---------------------+\n");
    }

    void printHCConnectionInfo() {
        HC::ConnectionDataStore.begin();
        HC::ConnectionData* conn = HC::ConnectionDataStore.load();

        Serial.printf("+---------------------------------------------+\n");
        Serial.printf("| Device Connection Data                      |\n");
        Serial.printf("+-----------------------+---------------------+\n");
        Serial.printf("| Data                  |Value                |\n");
        Serial.printf("+-----------------------+---------------------+\n");
        Serial.printf("| Wifi SSID             | %s\n", conn->wifiSSID);
        Serial.printf("| Wifi Password         | %s\n", conn->wifiPassword);
        Serial.printf("| HC Server URL         | %s\n", conn->serverUrl);
        Serial.printf("| HC Username           | %s\n", conn->hcUsername);
        Serial.printf("| HC Password           | %s\n", conn->hcPassword);
        Serial.printf("| HC Port               | %s\n", conn->hcPort);
        Serial.printf("| HC Home ID            | %s\n", conn->homeID);
        Serial.printf("| Status Topic          | %s\n", conn->statusTopic);
        Serial.printf("| Info Topic            | %s\n", conn->infoTopic);
        Serial.printf("| Command Topic         | %s\n", conn->commandTopic);
        Serial.printf("| Broadcast Cmd Topic   | %s\n", conn->broadcastCommandTopic);
        Serial.printf("+-----------------------+---------------------+\n");
    }

    void getDeviceUID(char* str) {
        uint64_t mac = ESP.getEfuseMac();

        itoa((mac & 0xff000000) >> 24, str, 16);
        str[2] = ':';
        itoa((mac & 0xff0000) >> 16, &str[3], 16);
        str[5] = ':';
        itoa((mac & 0xff00) >> 8, &str[6], 16);
        str[8] = ':';
        itoa((mac & 0xff), &str[9], 16);
    }

    int dump(void* data, long size) {
        unsigned int i;
        const unsigned char* const px = (unsigned char*)data;
        for (i = 0; i < size; ++i) {
            Serial.printf("%d > %02X(%c)\n", i, px[i], px[i]);
        }

        Serial.printf("\n");
        return 0;
    }
}
