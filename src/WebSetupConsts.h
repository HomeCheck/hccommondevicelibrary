#pragma once

#include "HCCommon.h"

namespace HC {
    char* const FILETYPE_HTML = "text/html";
    char* const FILETYPE_TXT = "text/plain";
    char* const FILETYPE_CSS = "text/css";
    char* const FILETYPE_JS = "text/javascript";
    char* const FILETYPE_JSON = "application/json";
    char* const FILETYPE_PNG = "image/png";
    char* const FILETYPE_JPG = "image/jpg";

    struct FileEntry {
        char* path;
        char* mimeType;
    };

    extern DeviceState defaultDeviceState;

    extern ConnectionData defaultConnectionData;
}
