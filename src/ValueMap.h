#pragma once

#include "HCCommon.h"
#include "Preferences.h"
#include <set>

using namespace std;

namespace HC {
    class ValueMap {
    private:
        Preferences* preferences;
    protected:
        std::set<String> allowedKeys;

        bool isAllowed(String name);
        /* if true, load/save is allowed, Preferences object will be initialized */
        bool allowPersistence;
        /* contains original data */
        ValuePairs params;
    public:
        ValuePairs defaults;
        bool onlyAllowed = true;
        char prefNamespace[16];

        ValueMap(const char* pNamespace = "");

        /* Sets the value (value) of a key (name) */
        void set(const char* name, const char* value);
        
        /* Adds a default value to a key (name) it will be used when no value is set otherwise */
        void addDefault(const char* name, const char* value);
        
        /* Adds new item to the allowed key list. When calling set method,
           if onlyAllowed is true (default) one can set key value only if key is in allowed keys list 
           Otherwise one can set any key */
        void addAllowedKey(const char* name);

        /* Both adds allowed key, and sets its default value  */
        void addAllowedKeyAndDefault(const char* name, const char* value);
        
        /* Returns the actual value of a key, if not yet set, returns default value */
        String get(const char* name);

        /* Removes key from list */
        void remove(const char*  name);

        /* Acquires the underlying map */
        ValuePairs* getAll();

        /* Sets all keys to their default values when default is set */
        void resetToDefaults();

        
        /* Load values from the persistent storage (nvm). It has effect when allowPersistence property 
           is set to true, and perfNamespace is set. If a key is not set, default value will be applied */
        void load();

        /* Saves values into persistent storage. It has effect when allowPersistence property is true
           and perfNamespace is set. */
        void save();

        ~ValueMap();
    };
}