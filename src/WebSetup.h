#pragma once

#include "WebSetupConsts.h"
#include "HCCommon.h"
#include "DeviceStateStore.h"
#include "ConnectionDataStore.h"
#include "WiFi.h"
#include "ESPAsyncWebServer.h"
#include "SPIFFS.h"
#include "ArduinoJson.h"

namespace HC {
    struct WebSetupConfig {
        char* SSID;
        char* password;
        IPAddress* localIP;
        IPAddress* gatewayIP;
        IPAddress* subnetMask;
        uint16_t port;
        ConnectionDataStoreClass* connectionDataStore; // 
        DeviceStateStoreClass* deviceStateStore; // reference to the common device state as a DI
    };

    class WebSetupClass {
    private:
        WebSetupConfig config;
        AsyncWebServer* server;
        StaticJsonDocument<JSON_BUFFER_SIZE> responseJsonDoc;

        ConnectionDataStoreClass* connectionDataStore; // 
        DeviceStateStoreClass* deviceStateStore;

        DeviceState* deviceState;
        ConnectionData* connectionData;

        const char* const JSON_MESSAGE = "message";
        const char* const JSON_STATUS = "status";

        const FileEntry availableFiles[7] = {
            {"/index.html", FILETYPE_HTML },
            {"/styles.css", FILETYPE_CSS },
            {"/script.js", FILETYPE_JS },
            {"/axios.min.js", FILETYPE_JS },
            {"/info128.png", FILETYPE_PNG },
            {"/setup128.png", FILETYPE_PNG },
            {"/reboot128.png", FILETYPE_PNG }
        };

        void setupWebServerFiles();
        void setupWebServerEntryPoints();
    protected:
    public:
        WebSetupClass(void);
        ~WebSetupClass();

        void begin(WebSetupConfig&);
        void setError(const DeviceErrorType errorCode, const char* reason); // make it a interface!!!
        void getConnectionData(AsyncWebServerRequest* request);
        void setConnectionData(AsyncWebServerRequest* request, uint8_t* data, size_t len, size_t index, size_t total);
        void getDeviceState(AsyncWebServerRequest* request);
        void getDeviceInfo(AsyncWebServerRequest* request);
        /* Endpoint to restart device in work mode */
        void doStart(AsyncWebServerRequest* request);
        /*  */
        void resetToDefaults(AsyncWebServerRequest* request);
        /* Sends HTTP 200 OK message as a response, in a standard json object */
        void sendOK(AsyncWebServerRequest* request, char* message);
        /* Sends the given error message in a standard JSON object */
        void sendError(AsyncWebServerRequest* request, int code, char* message);
        /* This function generates a working config object for WebSetup Class generation
        * This function needs to be rewritten as it only temporary
        * constants needs to be removed,etc.
        */
        WebSetupConfig createConfig();

    };

    extern WebSetupClass WebSetup;
}