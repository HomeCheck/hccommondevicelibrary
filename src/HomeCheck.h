#pragma once

#include "HCCommon.h";
#include "BaseDataStore.h"
#include "BaseDevice.h"
#include "Commander.h"
#include "ConnectionDataStore.h"
#include "DeviceStateStore.h"
#include "MessageComposer.h"
#include "WebSetup.h"
#include "WebSetupConsts.h"