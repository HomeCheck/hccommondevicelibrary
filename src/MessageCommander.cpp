#include "HCCommon.h"
#include "MessageCommander.h"

namespace HC {
    MessageCommanderClass::MessageCommanderClass(MessageCommanderConfig& config) :
        mqttClient(config.mqttClient) {
    }

    void MessageCommanderClass::addCommand(const char* commandName, CommandFn callback) {
        commands[commandName] = callback;
    }

    bool MessageCommanderClass::callCommand(const char* requestId, const char* commandName, const ValuePairs& values) {
        // whether we need to check command exist or not?
        CommandFn action = commands[commandName];

        return action(requestId, values);
    }

    bool MessageCommanderClass::hasCommand(const char* commandName) {
        return commands.count(commandName);
    }

    void MessageCommanderClass::printCommands() {
        log_d("Registered Commands: %d\n", commands.size());
        for (auto const& command : commands) {
            log_d("Command: %s",command.first.c_str());
        }
    }
}