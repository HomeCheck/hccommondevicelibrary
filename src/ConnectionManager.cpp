#include "HCCommon.h"
#include "ConnectionManager.h"
#include "ESP32Tools.h"

namespace HC {
    ConnectionManagerClass::ConnectionManagerClass(ConnectionManagerConfig& config) :
        mqttClient(config.mqttClient),
        wifiClient(config.wifiClient),
        deviceStateStore(config.deviceStateStore) {
        this->deviceState = deviceStateStore.get();
    }
    
    DeviceErrorType ConnectionManagerClass::connectToWifi(ConnectionData& data) {
        this->wifiClient.begin(data.wifiSSID, data.wifiPassword);
        int count = 0;
        int status = WiFi.status();
        log_d("Connecting to AP '%s' using password '%s'", data.wifiSSID, data.wifiPassword);

        while (status != WL_CONNECTED && count++ < wifi_connect_try_count) {
            Serial.print(".");
            status = WiFi.status();

            delay(wifi_connect_try_interval);
        }

        IPAddress ip = WiFi.localIP();

        log_d("Connected to wifi %d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);

        if (WiFi.status() == WL_CONNECTED) {
            log_d("Connected to Wifi");
            return DeviceErrorType::DEVICE_OK;
        }
        else {
            log_e("Connection to wifi failed");

            char* errorReason = "Unable to connect to Wifi AP";
            log_e("Error: %s", errorReason);

            deviceStateStore.setError(DEVICE_CONNECTION_ERROR, errorReason);
            deviceStateStore.save();

            return DeviceErrorType::DEVICE_CONNECTION_ERROR;
        }
    }

    DeviceErrorType ConnectionManagerClass::connectToMQTT(ConnectionData& data) {
        int count = 0;
        mqttClient.setServer(data.serverUrl, atoi(data.hcPort));
        mqttClient.setBufferSize(JSON_BUFFER_SIZE);

        uint32_t mqttConnectStart = millis();

        log_d("Connecting to mqtt server %s:%s@%s:% ...", data.hcPassword, data.hcUsername, data.serverUrl, data.hcPort);

        while (!mqttClient.connected() && count++ < mqtt_connect_try_count) {
            Serial.print(".");

            //getDeviceUID(this->deviceState->uid);

            if (!mqttClient.connect(this->deviceState->uid, data.hcUsername, data.hcPassword)) {
                delay(mqtt_connect_try_interval);
            }
        }
        Serial.println();

        if (mqttClient.connected()) {
            log_d("Connected to MQTT Server");
            return DeviceErrorType::DEVICE_OK;
        } 
        else 
        {
            char* errorReason = "Unable to connect to MQTT server";
            log_e("Connection Error: %s", errorReason);

            deviceStateStore.setError(DEVICE_CONNECTION_ERROR, errorReason);
            deviceStateStore.save();

            return DeviceErrorType::DEVICE_CONNECTION_ERROR;
        }
    }

    DeviceErrorType ConnectionManagerClass::connectToAll(ConnectionData& connData) {
        DeviceErrorType result = this->connectToWifi(connData);

        if (result != DeviceErrorType::DEVICE_OK) {
            return result;
        };

        return this->connectToMQTT(connData);
    }
}