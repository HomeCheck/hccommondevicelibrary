#pragma once

#include "HCCommon.h"
#include "MessageCommander.h"
#include "ConnectionDataStore.h"
#include "DeviceStateStore.h"
#include "MessageComposer.h"
#include "BaseDataStore.h"
#include "PubSubClient.h"
#include "WiFi.h"
#include "WebSetup.h"
#include "ConnectionManager.h"
#include "BaseMessageManager.h"
#include "ValueMap.h"

namespace HC {
    struct BaseDeviceConfig {
        WiFiClass& wifiClient;
        PubSubClient& mqttClient;
        MessageCommanderClass& messageCommander;
        MessageComposerClass& messageComposer;
        WebSetupClass& webSetup;
        ConnectionDataStoreClass& connectionDataStore;
        DeviceStateStoreClass& deviceStateStore;
        ConnectionManagerClass& connectionManager;
        BaseMessageManagerClass& messageManager;
    };

    class BaseDeviceClass {
    private:
        /* Connection Data used for connectiong to Wifi and HC Server */
        ConnectionData* connData;

        /* internal persistent state of the device (mode, error, etc.) */
        DeviceState* deviceState;

        /* *** decorator objects  */
        /* ESP32 wifi client from WiFi.h */
        WiFiClass& wifiClient;

        /* MQTT client */
        PubSubClient& mqttClient;

        /* MessageComposer creates HomeCheck compilant json messages for mqtt  */
        MessageComposerClass& messageComposer;

        /* commander receives and process serial commands */
        MessageCommanderClass& messageCommander;

        /* Persistent data store for connection data */
        ConnectionDataStoreClass& connectionDataStore;

        /* Persistent data store for device data (state,uid,mode) */
        DeviceStateStoreClass& deviceStateStore;

        /* Web Setup module to setup device */
        WebSetupClass& webSetup;

        /* build connection with wifi and mqtt */
        ConnectionManagerClass& connectionManager;

        /* subscribes to mqtt messages and responds basic messages */
        BaseMessageManagerClass& messageManager;
    protected:
        /* 
         * paramNames contains all the params the particular device may have 
         */
        // @TODO: implement in HCWAT-37
        static ValuePairs paramNames;
        /* List of params
         * It contains all the parameters that controls device's behavior
         * (for example: measure interval, trigger lightness, desired color,
         * minimum speed, lowest height)
         */
        ValueMap params = ValueMap("params"); // not &params because it needs to be created on startup and it's not a dependency

        /* Trait List
         * It contains all physical values measured by the device
         * for example: lightness, switch state, humidity, temperature
         */
        ValueMap traits = ValueMap("traits"); // not &traits because it needs to be created on startup and it's not a dependency

        /* Loads all permanent data:
         * - connection data
         * - device state
         * - device params
         *  
         */
        void loadPermanentData();
        /* Saves some permanent data. 
         * - device state
         * - device params
         * 
         * @TODO: is it needed to save params, or state only?
         */
        void savePermanentData();

        bool onCmdGetInfo(const char* requestId, const ValuePairs& params);
        bool onCmdGetStatus(const char* requestId, const ValuePairs& params);
        bool onCmdSetParams(const char* requestId, const ValuePairs& params);
    public:
        BaseDeviceClass(BaseDeviceConfig& config, char* type);

        /* Basid Device informations that change rarely */
        DeviceInfoMessage info;

        /*
        * Call it from default setup function, like:
        * void setup() {
        *     device.setup();
        * }
        */
        void setup();

        /**
        * Call it from default loop function
        */
        void loop();

        /**
        * Connects to wifi and MQTT. When fails, return value has the error code, =/= 0
        */
        void connectToAll();

        /**
        * does a measurement and updates traits
        */
        void measure();

        void setMode(DeviceMode mode);
        void setErrorCode(DeviceErrorType errorCode);
        void setErrorReason(char* errorReason);

        char* getUID();

        void hardReset();
    };
}