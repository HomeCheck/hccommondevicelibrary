#pragma once


#include <ESP.h>
#include <map>
#include <set>

namespace HC {
    typedef char* UID;

    /**
        * Data structure for device information, which will be
        * distributed to the server when asked
        * All fields are specific to the particular device
        * uid:    string representation of the device ID which is a
        *         4-byte unique identifier as 11:22:33:44
        * name:   NOT unique, readable name of the device like
        *         "Living room light"
        * params: Current Parameter settings of the device, stored as JSON string
        * firmwareVersion: major.minor.patch[.build] version of the firmware
        */
    struct DeviceInfoMessage {
        UID uid;
        char* name;
        char* type;
        char* firmwareVersion;
    };

    enum DeviceMode {
        DEVICE_MODE_DEFAULT = 0,
        DEVICE_MODE_SETUP = 1,
        DEVICE_MODE_WORK = 2,
        DEVICE_MODE_FAILURE = 4
    };

    /*
     * bit
     * 7: 1 - fatal
     * 6:
     * 5:
     * 4: 1 - connection related
     * 3-0:   error number, not specific
     */
    enum DeviceErrorType {
        DEVICE_OK = 0b00000000,
        DEVICE_FATAL_ERROR = 0b10000001,
        DEVICE_FATAL_INVALID_STATE = 0b10000010,
        DEVICE_FATAL_SENSOR_ERROR = 0b10000011,
        DEVICE_FATAL_ACTUATOR_ERROR = 0b10000100,
        DEVICE_GENERAL_HARDWARE_ERROR = 0b10000101,
        DEVICE_GENERAL_SOFTWARE_ERROR = 0b10000110,
        DEVICE_CONNECTION_ERROR = 0b10010001,
        DEVICE_WIFI_CONNECTION_ERROR = 0b10010010,
        DEVICE_MQTT_CONNECTION_ERROR = 0b10010011,
        DEVICE_INVALID_COMMAND_RECEIVED = 0b00000001,
        DEVICE_INVALID_STATE = 0b00000010,
        DEVICE_SENSOR_ERROR = 0b00000011
    };

    /* Data structures for data persistency stores */

    /**
     *
     */
    struct DeviceState {
        char uid[12];
        char name[64];
        DeviceMode mode;
        char errorReason[256];
        DeviceErrorType errorCode;
        char firmwareVersion[16];
    };

    char* const DeviceStateField_UID = "UID";
    char* const DeviceStateField_MODE = "mode";
    char* const DeviceStateField_ERROR_CODE = "errorCode";
    char* const DeviceStateField_ERROR_REASON = "errorReason";
    char* const DeviceStateField_NAME = "name";
    char* const DeviceStateField_FIRMWARE_VERSION = "firmwareVersion";

    char* const ConnectionDataField_WIFI_SSID = "wifiSSID";
    char* const ConnectionDataField_WIFI_PASSWORD = "wifiPassword";
    char* const ConnectionDataField_SERVER_URL = "serverUrl";
    char* const ConnectionDataField_HC_USERNAME = "hcUsername";
    char* const ConnectionDataField_HC_PASSWORD = "hcPassword";
    char* const ConnectionDataField_HC_PORT = "hcPort";
    char* const ConnectionDataField_HOME_ID = "homeID";
    char* const ConnectionDataField_STATUS_TOPIC = "statusTopic";
    char* const ConnectionDataField_INFO_TOPIC = "infoTopic";
    char* const ConnectionDataField_COMMAND_TOPIC = "commandTopic";
    char* const ConnectionDataField_BROADCAST_COMMAND_TOPIC = "broadcastCommandTopic";
  
    /**
     * All data from web setup
     */
    struct ConnectionData {
        char wifiSSID[64];              /* Wifi Access Point name */
        char wifiPassword[32];          /* Wifi AP password */
        char serverUrl[256];            /* HomeCheck Server URL without protocol,username,password or port number */
        char hcUsername[32];            /* Username for HomeCheck Server */
        char hcPassword[32];            /* Password for HomeCheck server */
        char hcPort[6];                 /* Port of the homecheck server (1883) */
        char homeID[64];                /* Unique Home ID of your home */
        char statusTopic[128];          /* topic name of the status messages sent by device (homecheck default: $homeID/$deviceUID/status/state) */
        char infoTopic[128];            /* topic name of the info messages sent by device (homecheck default: $homeID/$deviceUID/status/info) */
        char commandTopic[128];         /* topic name of the command messages sent by server to 
                                            this particular device (homecheck default: $homeID/$deviceUID/command/#) */
        char broadcastCommandTopic[128];/* topic name of the broadcast command messages sent 
                                            by server to all the devices (homecheck default: $homeId/device/command/#) */
    };

    /* Command Message Types */
    char* const CMD_GET_INFO = "get-info";
    char* const CMD_GET_STATUS = "get-status";
    char* const CMD_SET_PARAMS = "set-params";

    /* Topic Template Variable Constants */
    char* const TV_HOME_ID = "$homeID";
    char* const TV_DEVICE_UID = "$deviceUID";
    char* const TV_COMMAND = "$command";
    char* const TV_STATUS = "$status";



    /* Json Node Constants */
    char* const MN_META = "meta";
    char* const MN_DATA = "data";
    char* const MN_INFO = "info";
    char* const MN_STATE = "state";
    char* const MN_STATUS = "status";
    char* const MN_ERROR_CODE = "errorCode";
    char* const MN_ERROR_REASON = "errorReason";
    char* const MN_UID = "uid";
    char* const MN_REQUEST_ID = "requestid";
    char* const MN_NAME = "name";
    char* const MN_TYPE = "type";
    char* const MN_TRAITS = "traits";
    char* const MN_FIRMWARE_VERSION = "firmwareVersion";
    char* const MN_COMMAND = "command";
    char* const MN_PARAMS = "params";
    char* const MN_VALUE = "value";

    /* Buffer Sizes for transferring data */
    const uint32_t JSON_BUFFER_SIZE = 8192;

    /* Maximum length of a param. */
    const uint16_t MAX_DEVICE_PARAM_VALUE_LENGTH = 255;

    /* Maximum length of a param key, Can!t be more than 16 
       because of Preferences lib */
    const uint16_t MAX_DEVICE_PARAM_KEY_LENGTH = 16;



    /* Type for key-value string pairs like Traits, params etc. */
    typedef std::map<String, String> ValuePairs;

    /* Type for allowed Keys in a value list */
    typedef std::set<String> KeyList;
}