#pragma once

#include "BaseDataStore.h"
#include "HCCommon.h"

namespace HC {
    class DeviceStateStoreClass : public BaseDataStoreClass<DeviceState> {
    private:
    protected:
    public:
        DeviceMode getMode();
        void setMode(const DeviceMode);

        char* getUID();
        char* getName();

        DeviceErrorType getErrorCode();        
        char* getErrorReason();
        void setError(const DeviceErrorType errorCode, const char* reason);

        char* getFirmwareVersion();
        DeviceStateStoreClass(char* const name);
    };

    extern DeviceStateStoreClass DeviceStateStore;
}