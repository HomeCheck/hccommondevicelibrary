#pragma once

#include "HCCommon.h"
#include "PubSubClient.h"
#include "MessageComposer.h"
#include "MessageCommander.h"
#include "ConnectionDataStore.h"
#include "DeviceStateStore.h"
#include <map>

namespace HC {
    struct BaseMessageManagerConfig {
        PubSubClient& mqttClient;
        MessageComposerClass& messageComposer;
        MessageCommanderClass& messageCommander;
        ConnectionDataStoreClass& connectionDataStore;
        DeviceStateStoreClass& deviceStateStore;
    };

    class BaseMessageManagerClass {
    private:
        PubSubClient& mqttClient;
        MessageComposerClass& messageComposer;
        MessageCommanderClass& messageCommander;
        ConnectionDataStoreClass& connectionDataStore;
        DeviceStateStoreClass& deviceStateStore;
        ConnectionData* connectionData;
        DeviceState* deviceState;

        /* Replaces template placeholders to their actual values */
        void getTopicFromTemplate(String& topicTemplate, ValuePairs tmpPairs);

        /* Subscribes to a topic defined by topic template and key-value pairs 
           format is like:
           topic: "$homeID/$deviceUID/command/get-info" 
           values: "$homeID": "default", "$deviceUID": "22:33:44:55"
           result: "default/22:33:44:55/command/get-info"
         */
        void subscribeToTopic(const String& topic, ValuePairs tmpPairs);

        /* Callback handler when message arrives
           - Processes message: acquires requestId, command and params from message
           - looks up for required command and calls its handler function with the provided params
           - handles result
         */
        bool onMessageArrive(const char topic[], byte* payload, unsigned int length);
    public:
        BaseMessageManagerClass(BaseMessageManagerConfig& config);

        /* subscribes device to all the necessary topics (all the basic topics needed for all devices) */
        void subscribeToAll(); // TODO: subscribeall needs params for callbacks

        /* Adds a message to a queue for later publish.
           This method is necessary because PubSubClient has 1 buffer for inbound and outbound
           messages, so it's not possible to publish topic from an inbound message callback function
           without losing information.
           For now, only possible 1 message to send, no queue
         */
        void addMessageToPublish(const char topic[], const byte* payload, uint16_t length);

        /* publishes all the messages ueued before and empty queue. For now only 1 message is allowed (no queue)  */
        void publishMessages();

        /* Processes message payload and acquires all data from it, like:
           requestId, command and command parameters as key-value pairs */
        bool processMessage(
            String& requestId, 
            String& command, 
            ValuePairs& params, 
            const char topic[], 
            byte* payload, 
            unsigned int length
        );

        String getCommandTopic(const char* commandName);

        String getStatusTopic(const char* statusName);
    };
}
