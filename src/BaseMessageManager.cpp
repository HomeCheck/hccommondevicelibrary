#include "HCCommon.h"
#include "BaseMessageManager.h"
#include "ESP.h"
#include "ESP32Tools.h"
#include "ArduinoJson.h";

namespace HC {
    const char NULL_STR[] = "null";

    char* messageTopic;
    byte* messagePayload;
    uint16_t messageLength;
    bool hasMessageToPublish = false;

    BaseMessageManagerClass::BaseMessageManagerClass(BaseMessageManagerConfig& config) :
        mqttClient(config.mqttClient),
        messageCommander(config.messageCommander),
        messageComposer(config.messageComposer),
        connectionDataStore(config.connectionDataStore),
        deviceStateStore(config.deviceStateStore)
    {
        deviceState = deviceStateStore.get();
        connectionData = connectionDataStore.get();
    }

    void BaseMessageManagerClass::getTopicFromTemplate(String& topicTemplate, ValuePairs tmpPairs) {
        for (auto& tmpPair : tmpPairs) {
            topicTemplate.replace(tmpPair.first, tmpPair.second);
        }
    }

    void BaseMessageManagerClass::subscribeToTopic(const String& topicTemplate, ValuePairs tmpPairs) {
        String topic = topicTemplate;

        getTopicFromTemplate(topic, tmpPairs);

        log_d("Subscribe to %s", topic.c_str());
        this->mqttClient.subscribe(topic.c_str());

        this->mqttClient.setCallback([this](const char topic[], byte* payload, unsigned int length) {
            onMessageArrive(topic, payload, length);
        });
    }

    bool BaseMessageManagerClass::onMessageArrive(const char topic[], byte* payload, unsigned int length) {
        String requestId;
        String command;
        ValuePairs params;

        if (this->processMessage(requestId, command, params, topic, payload, length)) {
            if (messageCommander.hasCommand(command.c_str())) {
                return messageCommander.callCommand(requestId.c_str(), command.c_str(), params);
            }
            else {
                log_d("Command not found: %s", command);
                return false;
            }
        }
        else {
            // error handling should be here or elsewhere?
            return false;
        }        
    }

    void BaseMessageManagerClass::addMessageToPublish(const char topic[], const byte* payload, uint16_t length) {
        messageTopic = const_cast<char*>(topic);
        messagePayload = const_cast<byte*>(payload);
        messageLength = length;
        hasMessageToPublish = true;
        log_d("add message to queue: '%s'...", topic);
    }

    void BaseMessageManagerClass::publishMessages() {
        
        if (hasMessageToPublish) {
            log_d("publish...");
            mqttClient.publish(messageTopic, messagePayload, messageLength);
            hasMessageToPublish = false;
        }
    }

    void BaseMessageManagerClass::subscribeToAll() {
        ValuePairs tmpPairs {
            { "$homeID", this->connectionData->homeID },
            { "$deviceUID", this->deviceState->uid },
            { "$command", "#" }
        };

        // Command Topic is for sending commands from the server
        subscribeToTopic(String(this->connectionData->commandTopic), tmpPairs);

        // Broadcast Command Topic is for sending commands to all the devices from the server (switch off)
        subscribeToTopic(String(this->connectionData->broadcastCommandTopic), tmpPairs);        
    }

    bool BaseMessageManagerClass::processMessage(
        String& requestId,
        String& command,
        ValuePairs& params,
        const char topic[],
        byte* payload,
        unsigned int length
    ) {
        DynamicJsonDocument jsonDoc(JSON_BUFFER_SIZE);

        DeserializationError err = deserializeJson(jsonDoc, payload, length);

        if (err) {
            // logger.log("Error: unable to deserialize json: %s", conversionResult.c_str());
            log_e("Error: %s", err.c_str());
            return false;
        }

        requestId = jsonDoc[MN_META][MN_REQUEST_ID].as<String>();

        if (requestId == NULL_STR ) {
            // logger.log("Error: requestId is empty");
            log_d("Error: request ID is empty");
            return false;
        }

        command = jsonDoc[MN_DATA][MN_COMMAND].as<String>();

        if (command == NULL_STR) {
            // logger.log("Error: command is empty");
            log_d("Error: command is empty");
            return false;
        }

        JsonArray paramArray = jsonDoc[MN_DATA][MN_PARAMS].as<JsonArray>();

        for (JsonVariant item : paramArray) {
            JsonObject param = item.as<JsonObject>();
            String name = param[MN_NAME].as<String>();
            String value = param[MN_VALUE].as<String>();
            
            if (name.length() < 1) {
                // logger.log("Error: param name is empty");
                return false;
            } else {
                params[name] = value;
            }
        }

        return true;
    }

    String BaseMessageManagerClass::getCommandTopic(const char* commandName) {
        ValuePairs tmpPairs {
            { TV_HOME_ID, this->connectionData->homeID },
            { TV_DEVICE_UID, this->deviceState->uid },
            { TV_COMMAND, commandName }
        };

        String topic = connectionData->commandTopic;
        getTopicFromTemplate(topic, tmpPairs);

        return topic;
    }

    String BaseMessageManagerClass::getStatusTopic(const char* statusTopicName) {
        ValuePairs tmpPairs {
            { TV_HOME_ID, this->connectionData->homeID },
            { TV_DEVICE_UID, this->deviceState->uid },
            { TV_STATUS, statusTopicName }
        };

        String topic = connectionData->statusTopic;
        getTopicFromTemplate(topic, tmpPairs);

        return topic;
    }
}
