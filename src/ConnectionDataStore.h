#pragma once

#include "BaseDataStore.h"
#include "HCCommon.h"

namespace HC {
    typedef BaseDataStoreClass<ConnectionData> ConnectionDataStoreClass;

    extern ConnectionDataStoreClass ConnectionDataStore;
}