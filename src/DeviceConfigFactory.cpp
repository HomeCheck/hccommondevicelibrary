#include <ESP.h>
#include "HCCommon.h"
#include "WiFi.h"
#include "PubSubClient.h"

#include "BaseDevice.h"
#include "DeviceConfigFactory.h"

#include "MessageCommander.h"
#include "MessageComposer.h"
#include "DeviceStateStore.h"
#include "ConnectionDataStore.h"
#include "ConnectionManager.h"
#include "BaseMessageManager.h"
#include "WebSetup.h"

namespace HC {
    BaseDeviceConfig createDeviceConfig() {
        WiFiClient* wifiClient = new WiFiClient();
        PubSubClient* mqttClient = new PubSubClient(*wifiClient);


        MessageCommanderConfig msgCommCfg = {
            .mqttClient = *mqttClient
        };

        MessageCommanderClass* messageCommander = new MessageCommanderClass(msgCommCfg);

        MessageComposerConfig msgCompConfig = {
            .connectionDataStore = HC::ConnectionDataStore,
            .deviceStateStore = HC::DeviceStateStore
        };

        MessageComposerClass* messageComposer = new MessageComposerClass(msgCompConfig);

        ConnectionManagerConfig connManCfg = {
            .wifiClient = WiFi,
            .mqttClient = *mqttClient,
            .deviceStateStore = DeviceStateStore
        };

        ConnectionManagerClass* connectionManager = new ConnectionManagerClass(connManCfg);

        BaseMessageManagerConfig msgManCfg = {
            .mqttClient = *mqttClient,
            .messageComposer = *messageComposer,
            .messageCommander = *messageCommander,
            .connectionDataStore = ConnectionDataStore,
            .deviceStateStore = DeviceStateStore
        };

        BaseMessageManagerClass* messageManager = new BaseMessageManagerClass(msgManCfg);

        BaseDeviceConfig config = {
            .wifiClient = WiFi,
            .mqttClient = *mqttClient,
            .messageCommander = *messageCommander,
            .messageComposer = *messageComposer,
            .webSetup = WebSetup,
            .connectionDataStore = ConnectionDataStore,
            .deviceStateStore = DeviceStateStore,
            .connectionManager = *connectionManager,
            .messageManager = *messageManager
        };

        return config;
    }
}
