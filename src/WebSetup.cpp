#pragma once

#include "Esp.h"
#include "ESP32Tools.h"
#include "WebSetup.h"
#include "WebSetupConsts.h"
#include "ESPAsyncWebServer.h"
#include "WiFi.h"
#include "SPIFFS.h"
#include "ArduinoJson.h"

namespace HC {
    WebSetupClass::WebSetupClass(void) { }
    WebSetupClass::~WebSetupClass() {
        if (this->server) {
            delete this->server;
        }
    }

    /* It will set all the endpoints for ui client and related files that accessible from 
     * the ESP32. if also starts SPIFFS as a storage solution for these files. 
     * If SPIFFS can!t start, it will return without doing anything.
     * WebSetupClass::availableFiles variable contains all the files. */
    void WebSetupClass::setupWebServerFiles()
    {
        if (!SPIFFS.begin()) {
            this->setError(DeviceErrorType::DEVICE_GENERAL_HARDWARE_ERROR, "Unable to initialize internal filesystem");

            return;
        }

        int fileCount = sizeof(this->availableFiles) / sizeof(FileEntry);

        /* Server will provide files listed in availableFiles */
        for (uint8_t i = 0; i < fileCount; i++) {
            log_v("Adding file to server: %s", this->availableFiles[i].path);
            this->server->on(this->availableFiles[i].path, HTTP_GET, [i, this](AsyncWebServerRequest* request) {
                log_d("Serving request %s", this->availableFiles[i].path);
                request->send(
                    SPIFFS, this->availableFiles[i].path, this->availableFiles[i].mimeType);
                });
        }
    }

    /* It sets up all the entry points needed for 
       - read/update settings, info and state, 
       - handling 404
       - reset and start device */
    void WebSetupClass::setupWebServerEntryPoints()
    {
        this->server->onNotFound([](AsyncWebServerRequest* request) {
            request->send(404, "text/html", "<html><head></head><body><h1>HomeCheck.hu</h1><h2>404 Not Found</h2></body></html>");
            });

        this->server->on("/", HTTP_GET, [](AsyncWebServerRequest* request) {
            log_d("Serving request /");
            request->send(SPIFFS, "/index.html", FILETYPE_HTML);
            });

        this->server->on("/connection", HTTP_GET, [this](AsyncWebServerRequest* request) { this->getConnectionData(request); });

        this->server->on(
            "/connection",
            HTTP_POST,
            [](AsyncWebServerRequest* request) {},
            NULL,
            [this](AsyncWebServerRequest* request, uint8_t* data, size_t len, size_t index, size_t total) {
                this->setConnectionData(request, data, len, index, total);
            });

        this->server->on("/state", HTTP_GET, [this](AsyncWebServerRequest* request) { this->getDeviceState(request); });
        this->server->on("/info", HTTP_GET, [this](AsyncWebServerRequest* request) { this->getDeviceInfo(request); });
        this->server->on("/start", HTTP_POST, [this](AsyncWebServerRequest* request) { this->doStart(request); });
        this->server->on("/reset", HTTP_POST, [this](AsyncWebServerRequest* request) { this->resetToDefaults(request); });
    }

    /* It starts running web server, and does the following:
     * - start access point, sets up name and password
     * - starts web server, setup access points for files,operations and errors
     * - load saved configuration data and persistent device state
     */
    void WebSetupClass::begin(WebSetupConfig& config)
    {
        // Start WIFI Access Point
        WiFi.softAP(config.SSID, config.password, 1, false, 2);
        log_v("Access point started as %s", config.SSID);
        IPAddress IP = WiFi.softAPIP();

        log_d("AP IP address: %d.%d.%d.%d  ", IP[0], IP[1], IP[2], IP[3]);

        this->config = config;

        // Config and Start local Web server
        this->server = new AsyncWebServer(config.port);
        log_d("Server port: %d", config.port);

        setupWebServerFiles();
        setupWebServerEntryPoints();

        log_v("Starting web server...");
        this->server->begin();
        log_v("Web server started");

        // set store references and load their data
        this->connectionDataStore = this->config.connectionDataStore;
        this->connectionData = this->connectionDataStore->load();
        this->deviceStateStore = this->config.deviceStateStore;
        this->deviceState = this->deviceStateStore->load();
    }

    /* sets error state for the device. 
     * Depending on the error code, it will halt the device.
     * If fatal flag is not set, device won't stop
     * If fatal flag is set, it stops the device from running
     * 
     */
    void WebSetupClass::setError(const DeviceErrorType errorCode, const char* reason) {
        if ((errorCode | 0b10000000) > 0) {
            DeviceStateStoreClass& stateStore = *this->config.deviceStateStore;

            this->deviceStateStore->setError(errorCode, reason);

            log_e("FATAL ERROR (%d): %s", errorCode, reason);
        } else {
            log_e("NON-FATAL ERROR (%d): %s", errorCode, reason);
        }
    }

    /* getConnectionData returns the current connection data from memory
     * in JSON format.
     * method: POST
     */
    void WebSetupClass::getDeviceState(AsyncWebServerRequest* request) {
        log_d("request for GET /devicestate");
        StaticJsonDocument<JSON_BUFFER_SIZE> doc;
        String output;

        doc[DeviceStateField_UID] = this->deviceState->uid;
        doc[DeviceStateField_MODE] = this->deviceState->mode;
        doc[DeviceStateField_ERROR_CODE] = this->deviceState->errorCode;
        doc[DeviceStateField_ERROR_REASON] = this->deviceState->errorReason;
        doc[DeviceStateField_NAME] = this->deviceState->name;

        serializeJson(doc, output);

        request->send(200, FILETYPE_JSON, output);
    }

    void WebSetupClass::getDeviceInfo(AsyncWebServerRequest* request) {
        log_d("request for GET /info");

        String output; // is there any way to send response without output buffer?
        this->responseJsonDoc.clear();

        this->responseJsonDoc["deviceName"] = this->deviceState->name;
        this->responseJsonDoc["UID"] = this->deviceState->uid;
        this->responseJsonDoc["errorCode"] = this->deviceState->errorCode;
        this->responseJsonDoc["errorReason"] = this->deviceState->errorReason;
        this->responseJsonDoc["firmwareVersion"] = this->deviceState->firmwareVersion;

        switch (this->deviceState->mode) {
        case HC::DeviceMode::DEVICE_MODE_DEFAULT:
            this->responseJsonDoc["mode"] = "default";
            break;
        case HC::DeviceMode::DEVICE_MODE_FAILURE:
            this->responseJsonDoc["mode"] = "failure";
            break;
        case HC::DeviceMode::DEVICE_MODE_SETUP:
            this->responseJsonDoc["mode"] = "setup";
            break;
        case HC::DeviceMode::DEVICE_MODE_WORK:
            this->responseJsonDoc["mode"] = "work";
            break;
        default:
            this->responseJsonDoc["mode"] = "invalid";
            break;
        }

        serializeJson(this->responseJsonDoc, output);

        request->send(200, FILETYPE_JSON, output);
    }

    void WebSetupClass::getConnectionData(AsyncWebServerRequest* request) {
        log_d("request for GET /connection");
        this->connectionData = this->connectionDataStore->load();

        String output;

        this->responseJsonDoc.clear();
        this->responseJsonDoc[ConnectionDataField_WIFI_SSID] = this->connectionData->wifiSSID;
        this->responseJsonDoc[ConnectionDataField_WIFI_PASSWORD] = this->connectionData->wifiPassword;
        this->responseJsonDoc[ConnectionDataField_SERVER_URL] = this->connectionData->serverUrl;
        this->responseJsonDoc[ConnectionDataField_HC_USERNAME] = this->connectionData->hcUsername;
        this->responseJsonDoc[ConnectionDataField_HC_PASSWORD] = this->connectionData->hcPassword;
        this->responseJsonDoc[ConnectionDataField_HC_PORT] = this->connectionData->hcPort;
        this->responseJsonDoc[ConnectionDataField_HOME_ID] = this->connectionData->homeID;
        this->responseJsonDoc[ConnectionDataField_STATUS_TOPIC] = this->connectionData->statusTopic;
        this->responseJsonDoc[ConnectionDataField_INFO_TOPIC] = this->connectionData->infoTopic;
        this->responseJsonDoc[ConnectionDataField_COMMAND_TOPIC] = this->connectionData->commandTopic;
        this->responseJsonDoc[ConnectionDataField_BROADCAST_COMMAND_TOPIC] = this->connectionData->broadcastCommandTopic;

        serializeJson(this->responseJsonDoc, output);

        Serial.println(output);

        request->send(200, FILETYPE_JSON, output);
    }

    void WebSetupClass::setConnectionData(AsyncWebServerRequest* request, uint8_t* data, size_t len, size_t index, size_t total) {
        log_d("request for POST /connection");

        if (len > JSON_BUFFER_SIZE) {
            this->sendError(request, 413, "payload is too long");
            return;
        }

        this->responseJsonDoc.clear();
        deserializeJson(this->responseJsonDoc, data);
        JsonObject json = this->responseJsonDoc.as<JsonObject>();

        if (json[ConnectionDataField_WIFI_SSID] )

        strcpy(this->connectionData->wifiSSID, json[ConnectionDataField_WIFI_SSID]);
        strcpy(this->connectionData->wifiPassword, json[ConnectionDataField_WIFI_PASSWORD]);
        strcpy(this->connectionData->serverUrl, json[ConnectionDataField_SERVER_URL]);
        strcpy(this->connectionData->hcUsername, json[ConnectionDataField_HC_USERNAME]);
        strcpy(this->connectionData->hcPassword, json[ConnectionDataField_HC_PASSWORD]);
        strcpy(this->connectionData->hcPort, json[ConnectionDataField_HC_PORT]);
        strcpy(this->connectionData->homeID, json[ConnectionDataField_HOME_ID]);
        strcpy(this->connectionData->statusTopic, json[ConnectionDataField_STATUS_TOPIC]);
        strcpy(this->connectionData->infoTopic, json[ConnectionDataField_INFO_TOPIC]);
        strcpy(this->connectionData->commandTopic, json[ConnectionDataField_COMMAND_TOPIC]);
        strcpy(this->connectionData->broadcastCommandTopic, json[ConnectionDataField_BROADCAST_COMMAND_TOPIC]);

        this->connectionDataStore->save();
        this->sendOK(request, "Connection Data saved successfully");
    }

    void WebSetupClass::doStart(AsyncWebServerRequest* request) {
        log_d("request for GET /start");

        this->deviceState->mode = HC::DeviceMode::DEVICE_MODE_WORK;
        this->deviceStateStore->save();

        this->sendOK(request, "Device restart in WORK mode.");
        ESP.restart();
    }

    void WebSetupClass::resetToDefaults(AsyncWebServerRequest* request) {
        log_d("request for GET /reset");
        this->deviceState = &HC::defaultDeviceState;
        this->deviceStateStore->save();

        this->sendOK(request, "restored to default");
    }

    void WebSetupClass::sendOK(AsyncWebServerRequest* request, char* message) {
        this->responseJsonDoc.clear();
        this->responseJsonDoc["status"] = "OK";
        this->responseJsonDoc["message"] = message;

        String output;

        serializeJson(this->responseJsonDoc, output);

        request->send(200, FILETYPE_JSON, output);
    }

    void WebSetupClass::sendError(AsyncWebServerRequest* request, int code, char* message) {
        this->responseJsonDoc.clear();
        this->responseJsonDoc["status"] = "ERROR";
        this->responseJsonDoc["message"] = message;

        String output;
        serializeJson(this->responseJsonDoc, output);

        request->send(code, FILETYPE_JSON, output);
    }

    WebSetupConfig WebSetupClass::createConfig() {
        WebSetupConfig config = {
            .SSID = "device",
            .password = "error123",
            .localIP = nullptr,
            .gatewayIP = nullptr,
            .subnetMask = nullptr,
            .port = 80,
            .connectionDataStore = &ConnectionDataStore,
            .deviceStateStore = &DeviceStateStore
        };

        return config;
    }

    extern WebSetupClass WebSetup = WebSetupClass();
} // namespace HC