#pragma once

#include "ESP32Tools.h"
#include "BaseDevice.h"
#include "ConnectionDataStore.h"
#include "DeviceStateStore.h"
#include "HCCommon.h"
#include "WebSetup.h"
#include "ConnectionManager.h"
#include <map>

namespace HC {
    BaseDeviceClass::BaseDeviceClass(BaseDeviceConfig& config, char* type) :
        wifiClient(config.wifiClient),
        mqttClient(config.mqttClient),
        messageCommander(config.messageCommander),
        webSetup(config.webSetup),
        messageComposer(config.messageComposer),
        connectionDataStore(config.connectionDataStore),
        deviceStateStore(config.deviceStateStore),
        connectionManager(config.connectionManager),
        messageManager(config.messageManager)
    { 
        this->info.type = type;
        
        this->messageCommander.addCommand(CMD_GET_INFO, [this](const char* requestId, const ValuePairs& params) {
            return onCmdGetInfo(requestId, params);
        });

        this->messageCommander.addCommand(CMD_GET_STATUS, [this](const char* requestId, const ValuePairs& params) {
            return onCmdGetStatus(requestId, params);
        });

        this->messageCommander.addCommand(CMD_SET_PARAMS, [this](const char* requestId, const ValuePairs& params) {
            return onCmdSetParams(requestId, params);
            });

    }

    void BaseDeviceClass::setErrorCode(DeviceErrorType errorCode) {
        this->deviceState->errorCode = errorCode;
    }

    void BaseDeviceClass::setErrorReason(char* errorReason) {
        strcpy(this->deviceState->errorReason, errorReason);
    }

    void BaseDeviceClass::loadPermanentData() {
        this->connectionDataStore.begin();
        this->deviceStateStore.begin();
        this->connData = this->connectionDataStore.load();
        this->deviceState = this->deviceStateStore.load();

        this->params.load();
    }

    void BaseDeviceClass::savePermanentData() {
        this->deviceStateStore.save();
        this->params.save();
    }

    void BaseDeviceClass::setup() {
        log_d("Setup started");
        // The following data will be loaded at startup:
        // * connection data
        // * device state
        // * device params
        this->loadPermanentData();

        // Mode defines the behavior of the device
        // SETUP: device will start as a web server receiving requests from a browser
        // WORK: device will start normal mode, working as it is intended
        // FAILURE: device has unrecoverable error, can't start
        // DEFAULT: SETUP Mode
        DeviceMode mode =  this->deviceState->mode; //DEVICE_MODE_WORK; 

        // @TODO: info must be the source, not other way around!
        this->info.name = this->deviceState->name;
        this->info.firmwareVersion = this->deviceState->firmwareVersion;

        // result for connecting to wifi Access Point and MQTT
        // if either of them fails, connResult will contain error code
        DeviceErrorType connResult;

        // @TODO: check whether 
        getDeviceUID(this->deviceState->uid);
        this->info.uid = this->deviceState->uid;

        // set device state 
        // @TODO: remove and finish
        printESPInfo();
        printHCStatusInfo();
        printHCConnectionInfo();

        // this switch decides for the device how to work
        switch (mode) {
        case DeviceMode::DEVICE_MODE_WORK:
            log_d("Device Started in work mode");
            connResult = this->connectionManager.connectToAll(*this->connData);

            // @TODO: find out how to fallback when connection fails
            if (connResult != DeviceErrorType::DEVICE_OK) {
                this->deviceStateStore.setMode(DeviceMode::DEVICE_MODE_SETUP);
                this->deviceStateStore.save();
                log_d("***** fallback to setup");
            } else {
                this->messageManager.subscribeToAll();
            }
            
            break;

        case DeviceMode::DEVICE_MODE_FAILURE:
            // @TODO: figure out what error mode should do
            log_e("Fatal error occured, device is unable to start");
            log_e("Error Code: %d", this->deviceState->errorCode);
            log_e("Error Reason: %s", this->deviceState->errorReason);
            break;

        case DeviceMode::DEVICE_MODE_DEFAULT:
        case DeviceMode::DEVICE_MODE_SETUP:
        default:
            // @TODO: is there any better way?
            WebSetupConfig config = this->webSetup.createConfig();
            this->webSetup.begin(config);
            break;
        }
    }

    /**
     * Loop should be  called in main loop() 
     */
    void BaseDeviceClass::loop() {
        DeviceMode mode = DEVICE_MODE_WORK;// this->deviceState->mode;
        switch (mode) {
        case DeviceMode::DEVICE_MODE_WORK:
            this->mqttClient.loop(); // this is needed for proper mqtt message handling

            this->messageManager.publishMessages(); // this will publish all the messages 
                                                    // that were queued during message callbacks
            break;
        case DeviceMode::DEVICE_MODE_FAILURE:
            // @TODO: figure out what error mode should do
            // / blinking led? (there should be a dedicated port or method (A0/blinkLed())
            log_e("Fatal error occured, device is unable to start");
            log_e("Error Code: %d", this->deviceState->errorCode);
            log_e("Error Reason: %s", this->deviceState->errorReason);
            delay(10000);

            break;
        case DeviceMode::DEVICE_MODE_DEFAULT:
        case DeviceMode::DEVICE_MODE_SETUP:
        default:
            // nothing to do in setup or default mode
            break;
        }
    }

    void BaseDeviceClass::hardReset() {
        log_d("Resetting hardware...");
        log_d("Deleting persistent data");

        Preferences prefs;
        prefs.begin("HCConData");
        prefs.clear();

        Preferences prefs2;
        prefs2.begin("HCDeviceState");
        prefs2.clear();
    }

    bool BaseDeviceClass::onCmdGetInfo(const char* requestId, const ValuePairs& params) {
        String resultJson = this->messageComposer.createInfoResponse(this->info, this->params, const_cast<char*>(requestId));
        String statusInfoTopic = messageManager.getStatusTopic("info");

        this->messageManager.addMessageToPublish(statusInfoTopic.c_str(), (byte*)resultJson.c_str(), resultJson.length());
        
        return true;
    }
    
    bool BaseDeviceClass::onCmdGetStatus(const char* requestId, const ValuePairs& params) {
        String resultJson = this->messageComposer.createStatusResponse(
            *(this->deviceState), 
            (this->traits), 
            this->info.uid, 
            const_cast<char*>(requestId), 
            false
        );

        String statusTopic = messageManager.getStatusTopic("status");

        this->messageManager.addMessageToPublish(statusTopic.c_str(), (byte*)resultJson.c_str(), resultJson.length());
        return true;
    }

    bool BaseDeviceClass::onCmdSetParams(const char* requestId, const ValuePairs& params) {
        for (const auto& param : params) {
            if (param.first.length() > MAX_DEVICE_PARAM_KEY_LENGTH) {
                // log : too long device key
                log_d("setting param key '%s' too long (>%d)", param.first, MAX_DEVICE_PARAM_KEY_LENGTH);
            } else {
                this->params.set(param.first.c_str(), param.second.c_str());
            }
        }

        this->params.save();

        String resultJson = this->messageComposer.createInfoResponse(this->info, this->params, const_cast<char*>(requestId));
        String statusInfoTopic = messageManager.getStatusTopic("info");

        this->messageManager.addMessageToPublish(statusInfoTopic.c_str(), (byte*)resultJson.c_str(), resultJson.length());

    }
}
