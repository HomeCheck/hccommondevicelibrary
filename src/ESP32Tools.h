#pragma once

#include "Print.h"
#include "BaseDevice.h"

namespace HC {
    /* Esp specific info using log_d.  */
    void printESPInfo();

    /* HomeCheck Connection spcific info using log_d */
    void printHCConnectionInfo();

    /* HomeCheck Device Status info (UID, mode, error, status, reason, etc) */
    void printHCStatusInfo();

    /* Returns hardwired device unique id */
    void getDeviceUID(char*);

    /* hex dump of myStruct, number of bytes is defined by size param */
    int dump(void* myStruct, long size);
}
