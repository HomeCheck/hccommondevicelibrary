#pragma once

#include <Cli.h>
#include <Esp.h>

namespace HC {
    CLIClass::CLIClass() {
        this->reset();
    }

    void CLIClass::reset() {
        for (uint8_t i = 0; i < CLIClass::maxParams; i++) {
            this->param[i][0] = 0;
            this->nParam[i] = 0;
        }
    }

    bool CLIClass::processSerial() {
        if (!Serial.available()) {
            return false;
        }

        uint8_t paramIndex = 0;

        while (Serial.available()) {
            char currentChar = '\0';
            char lastChar = separator;
            uint8_t pos = 0;

            while (Serial.available() && (currentChar != separator)) {
                lastChar = currentChar;
                currentChar = Serial.read();

                if (currentChar != separator) {
                    this->param[paramIndex][pos++] = currentChar;
                }
                else {
                    pos++;
                }
            }
            this->param[paramIndex][pos] = 0;
            this->nParam[paramIndex] = atoi(this->param[paramIndex]);

            paramIndex++;
        }

        this->currentParamCount = paramIndex;

        return true;

    }

    char* CLIClass::getCommand() {
        return &this->param[0][0];
    }

    char* CLIClass::getCmd() {
        return this->getCommand();
    }

    char* CLIClass::getParam(uint8_t paramIndex) {
        return &this->param[paramIndex][0];
    }

    uint32_t CLIClass::getParamAsNumber(uint8_t paramIndex) {
        return this->nParam[paramIndex];
    }

    extern CLIClass CLI = CLIClass();
}