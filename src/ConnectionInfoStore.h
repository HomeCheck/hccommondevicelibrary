#pragma once

#include "HCCommon.h"
#include "ValueMap.h"
#include <set>

namespace HC {
    class ConnectionInfoStoreClass : public ValueMap {
    private:
    protected:
        KeyList allowedKeys{"1", "2"};
    public:
        ConnectionInfoStoreClass();
    };
}