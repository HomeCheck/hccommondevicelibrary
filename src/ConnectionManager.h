#pragma once

#include "HCCommon.h"
#include "PubSubClient.h"
#include "WiFi.h"
#include "DeviceStateStore.h"

namespace HC {
    struct ConnectionManagerConfig {
        WiFiClass& wifiClient;
        PubSubClient& mqttClient;
        DeviceStateStoreClass& deviceStateStore;
    };

    class ConnectionManagerClass {
    private:
        PubSubClient& mqttClient;
        WiFiClass& wifiClient;
        DeviceStateStoreClass& deviceStateStore;
        DeviceState* deviceState;

        static const uint8_t wifi_connect_try_count = 20;      // times 
        static const uint16_t wifi_connect_try_interval = 1000; // ms
        static const uint32_t mqtt_connect_try_interval = 1000;    // ms
        static const uint16_t mqtt_connect_try_count = 20;     // times
    public:
        ConnectionManagerClass(ConnectionManagerConfig& config);

        /* connect to Wifi hotspot using param "data" */
        DeviceErrorType connectToWifi(ConnectionData& data);

        /* connect to MQTT hotspot using param "data" */
        DeviceErrorType connectToMQTT(ConnectionData& data);

        /* Connect to All */
        DeviceErrorType connectToAll(ConnectionData& data);
    };
}
