#pragma once

#include <stdint.h>

namespace HC {
    class CLIClass {
        static const uint8_t maxParams = 5;
        static const uint8_t maxParamLength = 32;
    private:
        char param[CLIClass::maxParams][CLIClass::maxParamLength];
        uint32_t nParam[CLIClass::maxParams];
    protected:    
    public:
        uint8_t currentParamCount;
        static const char separator = ' ';

        CLIClass();

        bool processSerial();
        void reset();
        char* getParam(uint8_t paramIndex);
        char* getCommand();
        char* getCmd();
        uint32_t getParamAsNumber(uint8_t paramIndex);
    };

    extern CLIClass CLI;
}