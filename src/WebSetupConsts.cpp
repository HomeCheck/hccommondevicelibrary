
#include "HCCommon.h"
#include "WebSetupConsts.h"

namespace HC {
    extern DeviceState defaultDeviceState = {
        {.UID = "uid" },
        {.name = "Name"} ,
        .mode = DeviceMode::DEVICE_MODE_SETUP,
        {.errorReason = "Reason" },
        .errorCode = HC::DeviceErrorType::DEVICE_OK,
        {.firmwareVersion = "1.0.0"}
    };

    extern ConnectionData defaultConnectionData = {
        {.wifiSSID = ""},
        {.wifiPassword = ""},
        {.serverUrl = ""},
        {.hcUsername = "<username>"},
        {.hcPassword = ""},
        {.hcPort = "1336"},
        {.homeID = "default-hc"},
        {.statusTopic = "/device/status"},
        {.infoTopic = "/device/info"},
        {.commandTopic = "/device/command"},
        {.brodacastCommandTopic = "/device/command"}
    };
}
