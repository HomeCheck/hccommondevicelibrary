#pragma once

#include <Preferences.h>
#include "HCCommon.h"

/* This header file has no cpp as templates needs to be included, and this includes both header and implementation */
/**
 *  BaseDataStore stored permanent data on flash drive of ESP32, SPIFFS
 */
namespace HC {
    template <typename DataStruct>
    class BaseDataStoreClass {
    private:
    protected:
        /* Internal object for storing key/value pairs, why not this class inherited from this one? */
        Preferences* preferences;
        
        /* Contains the main data needs persistence */
        DataStruct data;

        /* Variable name in preferences object where "data" will be stored */
        const char* dataName = "data";
        bool loaded = false;
        char name[16];
    public:
        BaseDataStoreClass(char* name);
        ~BaseDataStoreClass();
        void begin();
        DataStruct* load();
        DataStruct* get();

        void clear();
        void save(const DataStruct& data);
        void save(void);
    };

    /* -------------------------  Implementation  ------------------------------- */

    template <typename DataStruct>
    BaseDataStoreClass<DataStruct>::BaseDataStoreClass(char* name) {
        this->preferences = new Preferences(); // @TODO: make preferences a config param

        if (strlen(name) > 16) {
            log_e("Name must be less than 16 characters, given :%d", strlen(name));
            return;
        } 

        strcpy(this->name, name);
    }

    template <typename DataStruct>
    void BaseDataStoreClass<DataStruct>::begin() {
        /* preferences->begin fails to work when it's not called from setup function in .ino file */
        /* reason: 
            nvs_open in Preferences::begin will only work if nvs_flash_init is called. 
            You have to make sure nvs_flash_init is executed before your code.
            By arduino-esp32, nvs_flash_init is executed in initArduino which 
            is just before the setup function. */
        log_d("Prefs begin as '%d'", this->name);
        this->preferences->begin(this->name);
    }

    template <typename DataStruct>
    DataStruct* BaseDataStoreClass<DataStruct>::load() {
        uint16_t dataSize = sizeof(this->data);
        uint8_t data[dataSize];

        // getBytes return false if error happens (most of the time because data 
        // is not created in the time of calling)
        // so if getBytes fails, it tries to create data, then load again
        if (!this->preferences->getBytes(this->dataName, &this->data, dataSize)) {
            log_w("data '%s' does not exist, creating...", this->dataName);
            memset(&(this->data), 0, dataSize); // clear memory block using zeros
            this->save(this->data);
            this->preferences->getBytes(this->dataName, &this->data, dataSize);
        }

        return this->get();
    }

    template <typename DataStruct>
    DataStruct* BaseDataStoreClass<DataStruct>::get() {
        return &this->data;
    }

    template <typename DataStruct>
    void BaseDataStoreClass<DataStruct>::clear() {
        this->preferences->remove(this->dataName);
        this->load();
    }

    template <typename DataStruct>
    void BaseDataStoreClass<DataStruct>::save(const DataStruct& data) {
        this->preferences->putBytes(this->dataName, &data, sizeof(data));
    }

    template <typename DataStruct>
    void BaseDataStoreClass<DataStruct>::save() {
        this->save(this->data);
    }

    template <typename DataStruct>
    BaseDataStoreClass<DataStruct>::~BaseDataStoreClass() {
        delete this->preferences;
    }
}