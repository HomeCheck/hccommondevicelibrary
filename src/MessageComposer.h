#pragma once

#include "ArduinoJson.h"
#include "HCCommon.h"
#include "DeviceStateStore.h"
#include "ConnectionDataStore.h"
#include "ValueMap.h"

using namespace ArduinoJson;

namespace HC {
    struct MessageComposerConfig {
        ConnectionDataStoreClass& connectionDataStore;
        DeviceStateStoreClass& deviceStateStore;
    };

    class MessageComposerClass {
    private:
        ConnectionDataStoreClass& connectionDataStore;
        DeviceStateStoreClass& deviceStateStore;
    protected:
        void createEnvelope(JsonDocument& message);
        void addMeta(JsonDocument& message, const char* uid, const char* requestId);
    public:
        MessageComposerClass(MessageComposerConfig config);

        String createInfoResponse(const DeviceInfoMessage& infoData, ValueMap& params, const char* requestId);
        String createStatusResponse(const DeviceState& stateData, const ValueMap& traits, const char* uid, const char* requestId, bool busy = false);

        void addStatus(JsonDocument& message, const DeviceState& stateData, const char* requestId, bool busy);
        void addTraits(JsonDocument& message, ValueMap& traits);
    };
}

/*


{
    meta: {
        uid: string,
        requestId: string
    },
    data: {
        status: {
            errorCode: string|number
            errorReason: String,
            status: string
        },
        info: {
            uid: string,
            name: string,
            type: string,
            params: [
                {
                    name: string,
                    value: string
                }
            ]
            firmwareVersion: string
        },
        traits: []
    }
}

*/