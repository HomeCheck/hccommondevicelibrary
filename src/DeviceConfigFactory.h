#pragma once

#include "BaseDevice.h"

namespace HC {
    BaseDeviceConfig createDeviceConfig();
}
