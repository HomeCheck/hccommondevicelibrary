#pragma once

#include "DeviceStateStore.h"
#include "HCCommon.h"

namespace HC {
    DeviceStateStoreClass::DeviceStateStoreClass( char* const name) : BaseDataStoreClass<DeviceState>(name) { }

    DeviceMode DeviceStateStoreClass::getMode() {
        return this->data.mode;
    }

    void DeviceStateStoreClass::setMode(const DeviceMode mode) {
        this->data.mode = mode;
    }

    char* DeviceStateStoreClass::getUID() {
        return &(this->data).uid[0];
    }

    char* DeviceStateStoreClass::getName() {
        return &this->data.name[0];
    }

    DeviceErrorType DeviceStateStoreClass::getErrorCode() {
        return this->data.errorCode;
    }

    void DeviceStateStoreClass::setError(const DeviceErrorType errorCode, const char* reason) {
        this->data.errorCode = errorCode;

        strcpy(this->data.errorReason, reason);
        this->save();
    }

    char* DeviceStateStoreClass::getErrorReason() {
        return &this->data.errorReason[0];
    }

    char* DeviceStateStoreClass::getFirmwareVersion() {
        return &this->data.firmwareVersion[0];
    }
    
    extern DeviceStateStoreClass DeviceStateStore = DeviceStateStoreClass("HCDeviceState");
}