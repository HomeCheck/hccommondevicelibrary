#include "ArduinoJson.h";
#include "HCCommon.h"
#include "ConnectionDataStore.h"
#include "DeviceStateStore.h"
#include "MessageComposer.h"
#include "ValueMap.h"

namespace HC {
    MessageComposerClass::MessageComposerClass(MessageComposerConfig config):
        connectionDataStore(config.connectionDataStore),
        deviceStateStore(config.deviceStateStore) {

        // @TODO: produces error message, why?
        connectionDataStore.begin();
        connectionDataStore.load();

        deviceStateStore.begin();
        deviceStateStore.load();
    }

    void MessageComposerClass::createEnvelope(JsonDocument& message) {
        JsonObject meta = message.createNestedObject(MN_META);
        JsonObject data = message.createNestedObject(MN_DATA);
    }

    String MessageComposerClass::createInfoResponse(const DeviceInfoMessage& infoData, ValueMap& params, const char* requestId) {
        DynamicJsonDocument message(8192);
        String resultJson;

        createEnvelope(message);

        addMeta(message, infoData.uid, const_cast<char*>(requestId));

        JsonObject info = (message["data"]).createNestedObject("info");
        JsonArray paramsNode = info.createNestedArray("params");

        ValuePairs* paramPairs = params.getAll();

        for (const auto& param : *paramPairs) {
            auto paramObj = paramsNode.createNestedObject();
            paramObj["name"] = param.first;
            paramObj["value"] = param.second;
        }

        info[MN_UID] = infoData.uid;
        info[MN_NAME] = infoData.name;
        info[MN_TYPE] = infoData.type;
        info[MN_FIRMWARE_VERSION] = infoData.firmwareVersion;

        serializeJsonPretty(message, resultJson);

        return resultJson;
    }

    String MessageComposerClass::createStatusResponse(
        const DeviceState& stateData, 
        const ValueMap& traits, 
        const char* uid, 
        const char* requestId, 
        bool busy
    ) {
        DynamicJsonDocument message(JSON_BUFFER_SIZE);
        String resultJson;

        this->createEnvelope(message);
        this->addMeta(message, uid, requestId);
        this->addStatus(message, stateData, requestId, busy);
        this->addTraits(message, const_cast<ValueMap&>(traits));

        serializeJsonPretty(message, resultJson);

        return resultJson;
    }

    void MessageComposerClass::addMeta(JsonDocument& message, const char* uid, const char* requestId) {
        message[MN_META][MN_REQUEST_ID] = requestId;
        message[MN_META][MN_UID] = uid;
    }

    void MessageComposerClass::addStatus(JsonDocument& message, const DeviceState& stateData, const char* requestId, bool busy) {
        JsonObject status = (message[MN_DATA]).createNestedObject(MN_STATUS);

        status[MN_ERROR_CODE] = stateData.errorCode;
        status[MN_ERROR_REASON] = stateData.errorReason;
        status[MN_STATUS] = stateData.mode;
    }

    void MessageComposerClass::addTraits(JsonDocument& message, ValueMap& traits) {
        ValuePairs* traitMap = traits.getAll();

        JsonArray traitJsonArray = (message[MN_DATA]).createNestedArray(MN_TRAITS);

        for (const auto& trait : *traitMap) {
            auto traitJson = traitJsonArray.createNestedObject();
            traitJson[MN_NAME] = trait.first;
            traitJson[MN_VALUE] = trait.second;
        }
    }
}
