#pragma once

#include "ConnectionDataStore.h"
#include "HCCommon.h"

namespace HC {
    extern ConnectionDataStoreClass ConnectionDataStore = ConnectionDataStoreClass("HCConData");
}