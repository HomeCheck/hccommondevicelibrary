Write-Host "This will install necessary HomeCheck related libraries to Arduino IDE. Please run this script from PowerShell as administrator."
$defaultLibraryFolder = "E:\Projects\Arduino\libraries"
$defaultSourceFolder = "E:\Projects\HomeCheck.hu\DeviceLibraries"
$libraryFolder = Read-Host -Prompt "Enter Arduino Library folder ($defaultLibraryFolder)"

if ($libraryFolder -eq "") {
    $libraryFolder = $defaultLibraryFolder
}

$sourceFolder = Read-Host -Prompt "Enter HC Library folder ($defaultSourceFolder)"

if ($sourceFolder -eq "") {
    $sourceFolder = $defaultSourceFolder
    Write-Host "using default folder: $sourceFolder"
} else {
    Write-Host "using entered dir: $sourceFolder"
}

$folderList = Get-ChildItem -Path $defaultSourceFolder -Directory -Force -ErrorAction SilentlyContinue | Select-Object Name | ForEach-Object {$_.Name}

forEach($folder in $folderList) {
    Write-Host "creating folder: $libraryFolder\$folder"
    $symLinkPath = "$libraryFolder\$folder"
    $targetPath = "$sourceFolder\$folder"

    if (Test-Path $symLinkPath) {
        Write-Host "Folder: $symLinkPath already exists."
    } else {
        New-Item -ItemType SymbolicLink -Path $symLinkPath -Target $targetPath 
    }    
}
