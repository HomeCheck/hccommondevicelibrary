const http = require('http');
const fs = require('fs');
var pathLib = require('path');

console.info("Starting webserver...");
const settings = {
    home: "e:\\projects\\HomeCheck.hu\\DeviceLibraries\\data\\",
    port: 80
}

const rules = [
    {
        name: "serve file",
        matcher: /^\/$/,
        method: "GET",
        action: serveIndex
    }, {
        name: "serve file",
        matcher: /^\/[a-zA-Z\-0-9_]+(\.[a-zA-Z\-0-9_]+)+$/,
        method: "GET",
        action: serveFiles
    }, {
        name: "get info",
        matcher: /^\/info\/get$/,
        method: "GET",
        action: getInfo
    }, {
        name: "get connection data",
        matcher: /^\/connection\/get$/,
        method: "GET",
        action: getConnection
    }, {
        name: "set connection data",
        matcher: /^\/connection\/set$/,
        method: "POST",
        action: setConnection
    }, {
        name: "start device",
        matcher: /^\/start$/,
        method: "POST",
        action: startDevice
    }, {
        name: "reset device",
        matcher: /^\/reset$/,
        method: "POST",
        action: resetDevice
    }
];

const EXT = {
    HTML: ".html",
    TXT: ".txt",
    CSS: ".css",
    JS: ".js",
    JSON: ".json",
    JPG: ".jpg",
    PNG: ".png"
}


const mimeTypes = {
    [EXT.HTML]: "text/html",
    [EXT.TXT]: "text/plain",
    [EXT.CSS]: "text/css",
    [EXT.JS]: "text/javascript",
    [EXT.JSON]: "applcation/json",
    [EXT.JPG]: "image/jpeg",
    [EXT.PNG]: "image/png"
}

const server = http.createServer((req, res) => {
    const { method, url, headers } = req;
    let ruleFound = false;

    rules.every((rule) => {
        const ruleMatched = (rule.method === method && url.match(rule.matcher));

        if (url.match(rule.matcher)) {
            console.info(rule.method, method, url, rule.matcher);
        }

        if (ruleMatched) {
            log(req, `${req.method} ${req.url} rule: ${rule.name}`);
            try {
                rule.action(req, res);
            } catch (e) {
                res.writeHeader(200, { "Content-Type": mimeTypes[EXT.HTML] });
                res.write("<h2>internal error 500</h2>");
                res.write(`<pre>${e}</pre>`);
                res.end();
                log(`Internal Error: ${e}`);
            }

            ruleFound = true;
        };

        return !ruleMatched;
    });

    !ruleFound && log(req, "rule not found");
});

server.listen(settings.port);

console.info("Server is up and running...");

/**
 * Log an entry to the screen
 * 
 * @param {any} req
 * @param {any} message
 */
function log(req, message) {
    console.info(`${getFormattedDate()} - ${message}`);
}

/**
 * returns current date formatted
 */
function getFormattedDate() {
    const ct = new Date();
    return `${ct.getFullYear()}-${ct.getMonth() + 1}-${ct.getDate()} ${ct.getHours()}:${ct.getMinutes()}:${ct.getSeconds()}`;
}

function getMimeType(ext) {
    return mimeTypes[ext] || "text/plain";
}

/**
 * Handler for serving index.html
 * @param {any} req
 * @param {any} res
 */
function serveIndex(req, res) {
    req.url = "/index.html";
    serveFiles(req, res);
}

function serveFiles(req, res) {
    const filename = String(req.url).replaceAll(/[^a-zA-Z\-0-9_\.]*/g, "");
    const path = settings.home + filename;
    const mimeType = getMimeType(pathLib.extname(path));

    log(res, `providing file: ${filename}, mime type: ${mimeType}`);

    if (!fs.existsSync(path)) {
        res.writeHeader(404);
        res.write(`file path not found ${path}`);
        res.end();
        return;
    }

    let content = fs.readFileSync(path);

    res.writeHeader(200, { "Content-Type": mimeType });
    res.write(content);
    res.end();
}

function getConnection(req, res) {
    let content = fs.readFileSync("./mocks/connection.get.json");
    res.writeHeader(200, { "Content-Type": getMimeType(".json") });
    res.write(content);
    res.end();
}

function setConnection(req, res) {
    let content = fs.readFileSync("./mocks/connection.set.json");
    let statusCode =(Math.random() < .5) ? 405 : 200;

    res.writeHeader(statusCode, { "Content-Type": getMimeType(".txt") });
    res.write(content);
    res.end();
}

function startDevice(req, res) {
    let content = fs.readFileSync("./mocks/start.json");
    res.writeHeader(200, { "Content-Type": getMimeType(".txt") });
    res.write(content);
    res.end();
}

function resetDevice(req, res) {
    let content = fs.readFileSync("./mocks/reset.json");
    res.writeHeader(200, { "Content-Type": getMimeType(".txt") });
    res.write(content);
    res.end();
}

function getInfo(req, res) {
    let content = fs.readFileSync("./mocks/info.get.json");
    res.writeHeader(200, { "Content-Type": getMimeType(".txt") });
    res.write(content);
    res.end();
}