
document.onreadystatechange = function () {
    document.readyState == "interactive" && init();
}

const tabs = ["welcome", "info", "setup", "result", "start", "reset"];
const connectionFields = ["wifiSSID", "wifiPassword", "serverUrl", "hcUsername", "hcPassword", "hcPort", "homeID", "statusTopic", "infoTopic", "commandTopic", "broadcastCommandTopic"];
const infoFields = ["deviceName", "UID", "mode", "errorCode", "errorReason", "firmwareVersion"];

let defaultData = {};

async function init() {
    showTab(0);
    
    let connectionData = (await getConnectionData()).data;
    let infoData = (await getInfo()).data;

    fillForm(infoData, getInfoInput, infoFields);
    fillForm(connectionData, getConnectionInput, connectionFields);
}
//----------------------------------------------------------------------------------------------
function getEl(className) {
    return document.getElementsByClassName(className)[0];
}

function getConnectionInput(name) {
    const form = document.querySelector("form[name='setup']");
    return form.querySelector(`[name='${name}']`);
}

function getInfoInput(name) {
    const form = document.querySelector("form[name='info']");
    return form.querySelector(`[name='${name}']`);
}

function ifTabIsTheOne(tabEl, idx, tabIdentifier) {
    return (idx == tabIdentifier && typeof tabIdentifier == "number") ||
        (typeof tabIdentifier == "string" && tabEl.classList.contains(tabIdentifier));
}

function setVisibility(el, isShown) {
    el.style.display = (isShown) ? "block" : "none";
}

// -----------------------------------------------------------------------------------------------
function showTab(tabIdentifier) {
    tabs.forEach((tabName, idx) => {
        const tabEl = getEl(tabName);
        setVisibility(tabEl, ifTabIsTheOne(tabEl, idx, tabIdentifier));
    });
}

function showNotification(msg, isError) {
    let notificationEl = getEl("notification");
    if (isError) {
        notificationEl.classList.add('error')
    } else {
        notificationEl.classList.remove('error')
    }

    notificationEl.style.opacity = "0";
    notificationEl.style.display = "flex";

    setTimeout(() => {    
        notificationEl.style.opacity = "1";
    }, 200);
    
    const msgEl = notificationEl.querySelector(".message");
    
    msgEl.innerText = msg;
}

function hideNotification() {
    let notificationEl = getEl("notification");
    notificationEl.style.opacity = "0";
    setTimeout(() => {
        notificationEl.style.display = "none";
    }, 500);
}

// -----------------------------------------------------------------------------------------------
function getInfo() {
    return axios.get('/info').then((response) => {
        defaultData = response;
        return response;
    }).catch((err) => {
        showNotification(`Device error: unable to fetch data from the device.`, true);
    });
}


// -----------------------------------------------------------------------------------------------
function collectConnectionData() {
    return connectionFields.reduce((acc, field) => {
        acc[field] = getConnectionInput(field).value;
        return acc;
    }, {});
}

function sendConnectionData(data) {
    return axios.post('/connection', data)
        .then((result) => {
            processResult(result, "Data saved successfully");
        })
        .catch((err) => {
            console.info(err);
            showNotification(err, true);
        });
}

function getConnectionData() {
    return axios.get('/connection').then((response) => {
        defaultData = response;
        return response;
    }).catch((err) => {
        showNotification(`Device error: unable to fetch data from the device.`, true);
    });
}

function fillForm(data, fieldGetter, fieldsList) {
    fieldsList.every((fieldName) => {
        const field = fieldGetter(fieldName);
        
        if (field != null) {
            field.value = data[fieldName];
        } else {
            showNotification("Device Error: invalid data from device", true);
        }
        
        return field !== null;
    });
}

function processResult(result, message) {
    try {
        let status = result.data.status;

        if (status === "OK") {
            showNotification(message || "Successful");
        }
    } catch(e) {
        showNotification("Invalid response", true)
    }
}

/** 
 * Action handler methods 
 **/
function checkConnection() {
    showNotification("Not implemented yet");
}

function submitConnectionData(ev) {
    getEl("submit").disabled = true;
    let data = collectConnectionData();

    sendConnectionData(data).then(() => {
        getEl("submit").disabled = false;
    });

    ev.preventDefault();
    ev.stopPropagation();
}

function resetForm() {
    fillForm(defaultData);
}

function doStart() {
    axios.post("/start").then((res) => {
        processResult(res, "Device Started Successfully");
    }).catch((err) => {
        showNotification(`Device error: error during sending reboot request to device`);
    });
}

function doReset() {
    axios.post("/reset").then((res) => {
        processResult(res, "Device has been reset Successfully");
    }).catch((err) => {
        showNotification(`Device error: error during sending reset request to device`);
    });
}